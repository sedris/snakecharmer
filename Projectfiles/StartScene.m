//
//  CCScene+StartScene.m
//  SnakeCharmer
//
//  Created by Ranna Zhou on 1/24/14.
//
//

#import "StartScene.h"

@implementation StartScene{
	PlayerPeer *_PlayerPeer;
}


-(id) init {
    if ((self = [super init])) {
        CCLayerColor *backgroundLayer = [CCLayerColor layerWithColor:ccc4(174, 206, 139, 255)];
        [self addChild:backgroundLayer z:-1];
        
        UITextField *nameTextField = [[UITextField alloc] initWithFrame:CGRectMake(60, 165, 200, 90)];
        [nameTextField setDelegate:[[StartSceneController alloc] init]];
        [nameTextField setText:@""];
        [nameTextField setTextColor: [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0]];
        [[[CCDirector sharedDirector] view] addSubview:nameTextField];
        [nameTextField becomeFirstResponder];
        
        CCMenuItem *playButton = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"btn-play-off.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"btn-play-on.png"] target:self selector:@selector(playButtonTapped:)];
        CCMenu *menu = [CCMenu menuWithItems:playButton, nil];
        [menu alignItemsVertically];
        [self addChild:menu];
    }
    return self;
}

- (void)playButtonTapped:(CCMenuItem *)sender {
    //[[CCDirector sharedDirector] replaceScene: (CCScene*)[[GameScene alloc] init]];
    JoinSceneController *jsc = [[JoinSceneController alloc] init];
    
    // Took from a MakeGamesWithUs tutorial...will remove. Just for reference now:
    //Ghost uses interface builder to set up the text view. Here, we have to manually set up the text view
    /*gsc.textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 260, 320, 220)];
    gsc.textView.backgroundColor=[UIColor colorWithWhite:0.21 alpha:1];
    gsc.textView.textColor = [UIColor colorWithWhite:1 alpha:1];
    
    //gsc.definition = [[game objectForKey:@"gamedata"] objectForKey:@"lastdefinition"];
    [gsc setModalTransitionStyle:UIModalTransitionStylePartialCurl]; //set transition type - page curl
    gsc.view.backgroundColor = [UIColor colorWithWhite:0.21 alpha:1];
    
    [gsc.view addSubview:gsc.textView];*/
    
    //add view controller to CCDirector
    //[CCDirector.sharedDirector presentViewController:gsc animated:YES completion:nil];
    [[CCDirector sharedDirector] replaceScene: jsc.scene];
}


- (void)addPeer:(NSString *)peerName {
    
}

@end
