//
//  JoinSceneController.m
//  SnakeCharmer
//
//  Created by Sarah Edris on 1/29/14.
//
//

#import "JoinSceneController.h"

@interface JoinSceneController () {
    PlayerPeer *_playerPeer;
}

@end

@implementation JoinSceneController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.scene = [[JoinScene alloc] init];
        if (_playerPeer == nil) {
            _playerPeer = [[PlayerPeer alloc] init];
            _playerPeer.maxPeers = 3;
            [_playerPeer startAcceptingConnectionsForSessionID:SESSION_ID];
            
            NSLog(@"SESSION NAME: %@",_playerPeer.session.displayName);
        }
        //[self.scene changePeers:@"test"];
        //[self matchmakingClient:_playerPeer peerBecameAvailable:@"123"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PlayerPeerDelegate

- (void)matchmakingClient:(PlayerPeer *)client peerBecameAvailable:(NSString *)peerID
{
    // do something - display available peers
    [self.scene changePeers:peerID];

    
}

- (void)matchmakingClient:(PlayerPeer *)client peerBecameUnvailable:(NSString *)peerID
{
    // do something - remove unavailable peers
    [self.scene changePeers:peerID];
}

@end
