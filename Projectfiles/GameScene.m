//
//  GameScene.m
//  Snake
//
#import "GameScene.h"

GameData* data;

#define MAX_SPEED   10
#define BASE_SPEED  0.2


void ccDrawFilledCGRect( CGRect rect )
{
	CGPoint poli[]=
        {rect.origin,
        CGPointMake(rect.origin.x,rect.origin.y + rect.size.height),
        CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height),
        CGPointMake(rect.origin.x + rect.size.width,rect.origin.y)};
    
	ccDrawLine(poli[0], poli[1]);
	ccDrawLine(poli[1], poli[2]);
	ccDrawLine(poli[2], poli[3]);
	ccDrawLine(poli[3], poli[0]);
}

SnakePiece MakeSnakePiece(NSInteger x, NSInteger y) {
    SnakePiece piece;
    piece.x = x;
    piece.y = y;
    return piece;
}

@implementation GameScene
@synthesize score = score_;

- (id)init {
    if ((self = [super init])) {
        data = data = [GameData sharedData];
        
        [self.parent addChild:self];
        
        CCLayerColor *backgroundLayer = [CCLayerColor layerWithColor:ccc4(174, 206, 139, 255)];
        // we need to add the background layer to a Z index of -1 in order to be able to draw primitives
        // in the draw method. The primitives are [apparently] always drawn at Z index 0
        [self addChild:backgroundLayer z:-1];
        //self.isTouchEnabled = YES;
        
        // x, y, width, height
        gameAreaRect_ = CGRectMake(data.CELL_DIMENSION/2, data.Y_OFFSET+data.CELL_DIMENSION/2, data.CELL_DIMENSION*data.NUM_COLS, data.CELL_DIMENSION*data.NUM_ROWS);
        
        /*CCMenuItem *pauseButton = [CCMenuItemSprite itemFromNormalSprite:[CCSprite spriteWithSpriteFrameName:@"btn-pause-off.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"btn-pause-on.png"] target:self selector:@selector(pauseBtnTapped:)];
        CCMenu *menu = [CCMenu menuWithItems:pauseButton, nil];
        menu.position = ccp(400.0f, 17.0f);
        [self addChild:menu];*/
        snakeSprites_ = [[NSMutableArray alloc] init];
        
        levels_ = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Levels" ofType:@"plist"]];
        
        [self setScore:0];
        [self setLevel:1];

        srand ( time(NULL) );
        [self scheduleUpdate];
        [self setUpFoodPiece];
        gameState_ = GameStateRunning;
    }
    return self;
}

- (void)pauseBtnTapped:(CCMenuItem *)sender {
    
}

- (void)setUpFoodPiece {
    NSArray *foodTypes = [NSArray arrayWithObjects:@"snail.png",@"worm.png", nil];
    
    foodSprite_ = [CCSprite spriteWithSpriteFrameName:[foodTypes objectAtIndex:rand()%[foodTypes count]]];
    
    NSInteger col = 0;
    NSInteger row = 0;
    
    while (true) {
        col = rand() % data.NUM_COLS;
        row = rand() % data.NUM_ROWS;
        BOOL isColliding = NO;
        for (int i = 0; i < snakePieces_; i++) {
            if (snake_[i].x == col || snake_[i].y == row)
            {
                isColliding = YES;
            }
        }
        if (!isColliding)
        {
            foodSprite_.tag = col * 100 + row;
            break;
        }
    }
    [self addChild:foodSprite_];
    foodSprite_.position = CGPointMake(col * data.CELL_DIMENSION, data.Y_OFFSET + row * data.CELL_DIMENSION);
}

- (void)setScore:(NSInteger)score {
    score_ = score;
    if (!scoreLabelShadow_) {
        scoreLabelShadow_ = [CCLabelTTF labelWithString:@"Score: 0" fontName:@"Arial" fontSize:20.0f];
        scoreLabelShadow_.color = ccc3(135,159,106);
        scoreLabelShadow_.position = ccp(29.5, 294.5);
        scoreLabelShadow_.anchorPoint = ccp(0.0, 0.5);
        [self addChild:scoreLabelShadow_];
    }
    if (!scoreLabel_) {
        scoreLabel_ = [CCLabelTTF labelWithString:@"Score: 0" fontName:@"Arial" fontSize:20.0f];
        scoreLabel_.color = ccc3(26,26,13);
        scoreLabel_.position = ccp(31, 296);
        scoreLabel_.anchorPoint = ccp(0.0, 0.5);
        [self addChild:scoreLabel_];
    }
    [scoreLabelShadow_ setString:[NSString stringWithFormat:@"Score: %d", score_]];
    [scoreLabel_ setString:[NSString stringWithFormat:@"Score: %d", score_]];
}

- (void)setLevel:(NSInteger)level {
    level_ = level;
    if (!levelLabelShadow_) {
        levelLabelShadow_ = [CCLabelTTF labelWithString:@"Score: 0" fontName:@"Arial" fontSize:16.0f];
        levelLabelShadow_.color = ccc3(135,159,106);
        levelLabelShadow_.position = ccp(29.5, 13.5);
        levelLabelShadow_.anchorPoint = ccp(0.0, 0.5);
        [self addChild:levelLabelShadow_];
    }
    if (!levelLabel_) {
        levelLabel_ = [CCLabelTTF labelWithString:@"Score: 0" fontName:@"Arial" fontSize:16.0f];
        levelLabel_.color = ccc3(26,26,13);
        levelLabel_.position = ccp(31, 15);
        levelLabel_.anchorPoint = ccp(0.0, 0.5);
        [self addChild:levelLabel_];
    }
    [levelLabelShadow_ setString:[NSString stringWithFormat:@"Level %d", level_]];
    [levelLabel_ setString:[NSString stringWithFormat:@"Level %d", level_]];
    
    [self loadLevel:level];
}

- (void)loadLevel:(NSInteger)level {
    NSDictionary *levelDef = [levels_ objectAtIndex:level-1];
    NSLog(@"Loading level: %@", levelDef);
    for (int i=0; i<(int)[[levelDef objectForKey:@"snake"] count]; i++)
    {
        NSDictionary *piece = [[levelDef objectForKey:@"snake"] objectAtIndex:i];
        snake_[i] = MakeSnakePiece([[piece valueForKey:@"x"] intValue], [[piece valueForKey:@"y"] intValue]);
        if (i == 0)
        {
            direction_ = [[piece valueForKey:@"direction"] intValue];
        }
        [self snakeSpriteAtIndex:i].scale = 1;
    }
    
    snakePieces_ = [[levelDef objectForKey:@"snake"] count];
    remainingFoodPieces_ = [[levelDef valueForKey:@"foodPieces"] intValue];
}

- (bool)detectCollisionFromX:(NSInteger)x andY:(NSInteger)y {
    // walls
    if (x < 0 || x > data.NUM_COLS || y < 0 || y > data.NUM_ROWS) {
        return true;
    }
    
    // self - look at all pieces other than head
    for (int i = 1; i < snakePieces_; i++) {
        if (snake_[i].x == x && snake_[i].y == y) {
            return true;
        }
    }
    
    return false;
}

- (void)step {
    direction_ = nextDirection_;
    SnakePiece tmp = snake_[0];
    switch (direction_) {
        case UP:
            tmp.y++;
            break;
        case RIGHT:
            tmp.x++;
            break;
        case DOWN:
            tmp.y--;
            break;
        case LEFT:
            tmp.x--;
            break;
        default:
            break;
    }
    if ([self detectCollisionFromX: tmp.x andY: tmp.y]) {
        gameState_ = GameStateGameOver;
        NSLog(@"GAME OVER");
    }
    else {
        SnakePiece lastPiece = snake_[snakePieces_-1];
        for (int i = snakePieces_ - 1; i > 0; i--) {
            snake_[i] = snake_[i-1];
        }
        snake_[0] = tmp;
        if (foodSprite_.tag / 100 == snake_[0].x && foodSprite_.tag % 100 == snake_[0].y) {
            [self setScore:score_ + 1];
            snake_[snakePieces_] = lastPiece;
            snakePieces_++;
            remainingFoodPieces_--;
            [foodSprite_ removeFromParentAndCleanup:YES];
            if (remainingFoodPieces_) {
                [self setUpFoodPiece];
            }
            else {
                gameState_ = GameStateLevelOver;
            }
        }
    }
}

- (void)update:(ccTime)time {
    if (gameState_ == GameStateRunning) {
        accumulator += time;
        float speedStep = BASE_SPEED;// - BASE_SPEED/MAX_SPEED * currentSpeed_;
        while (accumulator >= speedStep) {
            [self step];
            accumulator -= speedStep;
        }
    }
    
    KKInput* input = [KKInput sharedInput];
    input.gestureTapEnabled = YES;
    CGPoint pos;
    
    if (input.gestureTapRecognizedThisFrame) {
        pos = input.gestureTapLocation;
        // clockwise
        if (pos.x >= data.WIDTH_WINDOW/2) {
            if (direction_ == UP) {
                nextDirection_ = RIGHT;
            } else if (direction_ == RIGHT) {
                nextDirection_ = DOWN;
            } else if (direction_ == DOWN) {
                nextDirection_ = LEFT;
            } else if (direction_ == LEFT) {
                nextDirection_ = UP;
            }
        } else {
            // counterclockwise 
            if (direction_ == UP) {
                nextDirection_ = LEFT;
            } else if (direction_ == RIGHT) {
                nextDirection_ = UP;
            } else if (direction_ == DOWN) {
                nextDirection_ = RIGHT;
            } else if (direction_ == LEFT) {
                nextDirection_ = DOWN;
            }
        }
    }
}

- (CCSprite *)snakeSpriteAtIndex:(NSUInteger)index {
    NSAssert(index <= [snakeSprites_ count], @"Oopsiee");
    if ([snakeSprites_ count] == index) {
        CCSprite *sprite = nil;
        if (index == 0) {
            sprite = [CCSprite spriteWithSpriteFrameName:@"snake-head.png"];
        }
        else {
            sprite = [CCSprite spriteWithSpriteFrameName:@"snake-body.png"];
        }
        [snakeSprites_ addObject:sprite];
    }
    return [snakeSprites_ objectAtIndex:index];
}

-(void) draw {
	//glDisable(GL_LINE_SMOOTH);
	//glLineWidth( 1.0f );
	//glColor4ub(0,0,0,255);
	ccDrawFilledCGRect(gameAreaRect_);
    
    for (int i = 0; i < snakePieces_; i++) {
        CCSprite *sprite = [self snakeSpriteAtIndex:i];

        sprite.position = CGPointMake(snake_[i].x * data.CELL_DIMENSION, data.Y_OFFSET + snake_[i].y * data.CELL_DIMENSION);
        if (i == 0) {
            sprite.rotation = direction_ * 90;
        }
        if (![sprite parent]) {
            [self addChild:sprite];
        }
    }
}

- (void)dealloc {
    //[levels_ release];
    //[super dealloc];
}

@end
