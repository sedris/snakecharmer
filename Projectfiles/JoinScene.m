//
//  JoinScene.m
//  SnakeCharmer
//
//  Created by Sarah Edris on 1/29/14.
//
//

#import "JoinScene.h"

@implementation JoinScene
-(id) init {
    if ((self = [super init])) {
        CCLayerColor *backgroundLayer = [CCLayerColor layerWithColor:ccc4(174, 206, 139, 255)];
        [self addChild:backgroundLayer z:-1];
        peerName = @"";
        peerLabel = [CCLabelTTF labelWithString:@"Peers:" fontName:@"Arial" fontSize:20.0f];
        peerLabel.color = ccc3(26,26,13);
        peerLabel.position = ccp(31, 296);
        peerLabel.anchorPoint = ccp(0.0, 0.5);
        [self addChild:peerLabel];
        [peerLabel setString:[NSString stringWithFormat:@"Peers: %@", peerName]];
    }
    return self;
}

-(void) changePeers:(NSString *)p {
    peerName = p;
    [peerLabel setString:[NSString stringWithFormat:@"Peers: %@", p]];

}

@end
