//
//  PlayerPeer.h
//  SnakeCharmer
//
//  Created by Ranna Zhou on 1/26/14.
//
//

#import <Foundation/Foundation.h>

@class PlayerPeer;

@protocol PlayerPeerDelegate <NSObject>

- (void)matchmakingClient:(PlayerPeer *)client peerBecameAvailable:(NSString *)peerID;
- (void)matchmakingClient:(PlayerPeer *)client peerBecameUnavailable:(NSString *)peerID;

@end

@interface PlayerPeer : NSObject <GKSessionDelegate>

@property (nonatomic, assign) int maxPeers;
@property (nonatomic, strong, readonly) NSArray *connectedPeers;
@property (nonatomic, strong, readonly) GKSession *session;
@property (nonatomic, weak) id <PlayerPeerDelegate> delegate;

- (void)startAcceptingConnectionsForSessionID:(NSString *)sessionID;
- (NSUInteger)availablePeerCount;
- (NSString *)peerIDForAvailablePeerAtIndex:(NSUInteger)index;
- (NSString *)displayNameForPeerID:(NSString *)peerID;

@end
