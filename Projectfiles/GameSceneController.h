//
//  GameSceneController.h
//  SnakeCharmer
//
//  Created by Ranna Zhou on 1/27/14.
//
//

#import "GameScene.h"
#import "PlayerPeer.h"

@interface GameSceneController : UIViewController {
}

@property CCScene *scene;

@end