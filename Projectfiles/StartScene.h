//
//  CCScene+StartScene.h
//  SnakeCharmer
//
//  Created by Ranna Zhou on 1/24/14.
//
//

#import "CCScene.h"
#import "StartSceneController.h"
#import "JoinSceneController.h"

@interface StartScene : CCScene {
}

- (void)addPeer:(NSString *)peerName;

@end
