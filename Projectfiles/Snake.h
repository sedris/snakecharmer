//
//  Snake.h
//  SnakeCharmer
//
//  Created by Sarah Edris on 1/20/14.
//
//

#import <Foundation/Foundation.h>
#import "GameData.h"

typedef enum {
    RIGHT,
    DOWN,
    LEFT,
    UP
} Direction;

@interface Snake : CCNode {
    GameData *data;
    
    NSMutableArray* segments;
    #define INIT_LENGTH 3
    
    Direction direction;
    Direction nextDirection;
    Direction nextNextDirection;
    
    float speed;
}

-(id) init;
-(void) update:(ccTime)dt;
-(void) turnClockwise: (bool) clockwise;
-(void) changeLength: (int) delta;
-(CGPoint) getHead;

@end
