//
//  PlayerPeer.m
//  SnakeCharmer
//
//  Created by Ranna Zhou on 1/26/14.
//
//

#import "PlayerPeer.h"

@implementation PlayerPeer {
	NSMutableArray *_connectedPeers;
    NSMutableArray *_availablePeers;
}

@synthesize maxPeers = _maxPeers;
@synthesize session = _session;
@synthesize delegate = _delegate;

- (void)startAcceptingConnectionsForSessionID:(NSString *)sessionID {
/* 
    NOTES:
    http://nathanmock.com/files/com.apple.adc.documentation.AppleiOS6.0.iOSLibrary.docset/Contents/Resources/Documents/#documentation/NetworkingInternet/Conceptual/GameKit_Guide/GameKitConcepts/GameKitConcepts.html
    send data using: (bool)sendDataToAllPeers:(NSData *)data withDataMode:(GKSendDataMode)mode error:(NSError **)error
    largest message allowed is 87 kilobytes
 */

	_connectedPeers = [NSMutableArray arrayWithCapacity:self.maxPeers];
    _availablePeers = [NSMutableArray arrayWithCapacity:self.maxPeers];
    
    // TODO: give distinct displayName?
	_session = [[GKSession alloc] initWithSessionID:sessionID displayName:nil sessionMode:GKSessionModePeer];
	_session.delegate = self;
	_session.available = YES;
}

- (NSArray *)connectedPeers {
	return _connectedPeers;
}

- (NSArray *)availablePeers {
	return _availablePeers;
}

- (NSUInteger)availablePeerCount {
    return [_availablePeers count];
}

- (NSString *)peerIDForAvailablePeerAtIndex:(NSUInteger)index {
    return [_availablePeers objectAtIndex:index];
}

- (NSString *)displayNameForPeerID:(NSString *)peerID {
    return [_session displayNameForPeer:peerID];
}

#pragma mark - GKSessionDelegate
- (void)session:(GKSession *)session peer:(NSString *)peerID didChangeState:(GKPeerConnectionState)state {
	NSLog(@"PlayerPeer: peer %@ changed state %d", peerID, state);
    
    switch (state)
	{
        // Client discovered a new peer
		case GKPeerStateAvailable:
			if (![_availablePeers containsObject:peerID])
			{
				[_availablePeers addObject:peerID];
				[self.delegate matchmakingClient:self peerBecameAvailable:peerID];
			}
			break;
            
        // Client sees that the peer goes away
		case GKPeerStateUnavailable:
			if ([_availablePeers containsObject:peerID])
			{
				[_availablePeers removeObject:peerID];
				[self.delegate matchmakingClient:self peerBecameAvailable:peerID];
			}
			break;
            
		case GKPeerStateConnected:
			break;
            
		case GKPeerStateDisconnected:
			break;
            
		case GKPeerStateConnecting:
			break;
	}
}


- (void)session:(GKSession *)session didReceiveConnectionRequestFromPeer:(NSString *)peerID {
	NSLog(@"PlayerPeer: connection request from peer %@", peerID);
    NSLog(@"Peer name %@", [session displayNameForPeer:peerID]);

    
    // want to accept with acceptConnectionFromPeer:error: or deny with denyConnectionFromPeer:
}

- (void)session:(GKSession *)session connectionWithPeerFailed:(NSString *)peerID withError:(NSError *)error {
	NSLog(@"PlayerPeer: connection with peer %@ failed %@", peerID, error);
}

- (void)session:(GKSession *)session didFailWithError:(NSError *)error {
	NSLog(@"PlayerPeer: session failed %@", error);
}

- (void)setDataReceiveHandler:(id)handler withContext:(void *)context
{
    // TODO: create handler to handle what to do when receiving data from other peers
    // handler must implement:
    // - (void) receiveData:(NSData *)data fromPeer:(NSString *)peer inSession: (GKSession *)session context:(void *)context;
}

@end