//
//  GameLayer.m
//  SnakeCharmer
//
//  Created by Sarah Edris on 1/19/14.
//
//

#import "GameLayer.h"

@implementation GameLayer

GameData* data;

Snake *snake;
CGPoint food;
int score;

-(id) init {
	if ((self = [super init])) {
        snake = [[Snake alloc] init];
        [self addChild:snake];
        
        data = [GameData sharedData];
        
        [self scheduleUpdate];
    }
	return self;
}

-(void) draw {
    //draw rectangle
    ccColor4F rectColor = ccc4f(0.5, 0.5, 0.5, 1.0); //parameters correspond to red, green, blue, and alpha (transparancy)
    ccDrawSolidRect(ccp(0,0), ccp(data.WIDTH_WINDOW, data.HEIGHT_WINDOW), rectColor);
    
    //draw row lines
    ccDrawColor4F(.5, 0, 1.0, 1.0);
    int numRows = (data.HEIGHT_WINDOW - data.Y_OFFSET)/data.CELL_DIMENSION;
    int numCols = data.WIDTH_WINDOW/data.CELL_DIMENSION;
    for(int row = 0; row <= numRows; row++) {
        ccDrawLine(ccp(0, row*data.CELL_DIMENSION + data.Y_OFFSET), ccp(numCols*data.CELL_DIMENSION, row*data.CELL_DIMENSION + data.Y_OFFSET));
    }
    
    //draw column lines
    for(int col = 0; col <= numCols; col++) {
        ccDrawLine(ccp(col*data.CELL_DIMENSION, 0 + data.Y_OFFSET), ccp(col*data.CELL_DIMENSION, numRows*data.CELL_DIMENSION + data.Y_OFFSET));
    }
}

- (void) update:(ccTime)dt {
    KKInput* input = [KKInput sharedInput];
    input.gestureTapEnabled = YES;
    CGPoint pos;

    // right half = CW
    // left half = CCW
    // double taps will slow single tap recognition
    if (input.gestureTapRecognizedThisFrame) {
        pos = input.gestureTapLocation;
        NSLog(@"tap has been recognized!");
        if (pos.x >= data.WIDTH_WINDOW/2) {
            [snake turnClockwise:true];
        } else {
            [snake turnClockwise:false];
        }
    }
    
}

@end
