//
//  JoinSceneController.h
//  SnakeCharmer
//
//  Created by Sarah Edris on 1/29/14.
//
//

#import "PlayerPeer.h"
#import "JoinScene.h"

@interface JoinSceneController : UIViewController <PlayerPeerDelegate> {
}

@property JoinScene *scene;

@end
