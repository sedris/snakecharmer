//
//  GameLayer.h
//  SnakeCharmer
//
//  Created by Sarah Edris on 1/19/14.
//
//

#import "CCLayer.h"
#import "GameData.h"
#import "Snake.h"

@interface GameLayer : CCLayer {
}

-(id) init;
-(void) update:(ccTime)dt;
-(void) draw;

@end
