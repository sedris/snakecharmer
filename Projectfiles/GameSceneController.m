//
//  GameSceneController.m
//  SnakeCharmer
//
//  Created by Ranna Zhou on 1/27/14.
//
//

#import "GameSceneController.h"

@implementation GameSceneController

@synthesize scene;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.scene = [[GameScene alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // currently not called because not using presentViewController in startScene
}
@end
