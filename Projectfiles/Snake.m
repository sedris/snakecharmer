//
//  Snake.m
//  SnakeCharmer
//
//  Created by Sarah Edris on 1/20/14.
//
//

#import "Snake.h"

@implementation Snake:CCNode

-(id) init {
    if ((self = [super init]))
	{
        data = [GameData sharedData];
        speed = 10;
        
        segments = [[NSMutableArray alloc] init]; //this initializes the array
        // head of snake at index 0
        for (int k = 0; k < INIT_LENGTH; ++ k)
        {
            CCSprite *segment = [CCSprite spriteWithFile: @"dot.png"];
            segment.position = ccp(1.0/2*data.WIDTH_WINDOW + 1.0/2*data.CELL_DIMENSION, floorf(1.0/3*data.HEIGHT_WINDOW/data.CELL_DIMENSION)*data.CELL_DIMENSION - k*data.CELL_DIMENSION + 1.0/2*data.CELL_DIMENSION);
            [segments addObject:segment];
            [self addChild:segment];
            // add
            //[segments addObject:[NSValue value:&segment withObjCType:@encode(struct CGPoint)]];
        }
        //CCSprite *segment = [CCSprite spriteWithFile: @"dot.png"];
        //segment.position = ccp(10, 3 * data.CELL_DIMENSION + 70);
        //[segments addObject:segment];
        //[self addChild:segment];
        direction = UP;
        nextDirection = UP;
        nextNextDirection = UP;
        [self scheduleUpdate];
    }
    return self;
}

- (void) update:(ccTime)dt {
    // TODO: check collision with ]self
    
    CCSprite *head = [CCSprite spriteWithFile: @"dot.png"];//[segments objectAtIndex:[segments count] - 1];
    CCSprite *first = [segments objectAtIndex:0];

    CGPoint oldHeadPosition = ccp(first.position.x, first.position.y);
    
    if (nextDirection != direction) {
        if (nextDirection == UP) {
            head.position = ccp((oldHeadPosition.x/data.CELL_DIMENSION)*data.CELL_DIMENSION+1.0/2*data.CELL_DIMENSION, oldHeadPosition.y + speed * dt);
        } else if (nextDirection == RIGHT) {
            head.position = ccp(oldHeadPosition.x + speed * dt, ((oldHeadPosition.y-data.Y_OFFSET)/data.CELL_DIMENSION)*data.CELL_DIMENSION+1.0/2*data.CELL_DIMENSION+data.Y_OFFSET);
        } else if (nextDirection == DOWN) {
            head.position = ccp((oldHeadPosition.x/data.CELL_DIMENSION)*data.CELL_DIMENSION+1.0/2*data.CELL_DIMENSION, oldHeadPosition.y - speed * dt);
        } else if (nextDirection == LEFT) {
            head.position = ccp(oldHeadPosition.x - speed * dt, ((oldHeadPosition.y-data.Y_OFFSET)/data.CELL_DIMENSION)*data.CELL_DIMENSION+1.0/2*data.CELL_DIMENSION+data.Y_OFFSET);
        }
        direction = nextDirection;
        nextDirection = nextNextDirection;
    } else {
        if (direction == UP) {
            head.position = ccp(oldHeadPosition.x, oldHeadPosition.y + speed * dt);
        } else if (direction == RIGHT) {
            head.position = ccp(oldHeadPosition.x + speed * dt, oldHeadPosition.y);
        } else if (direction == DOWN) {
            head.position = ccp(oldHeadPosition.x, oldHeadPosition.y - speed * dt);
        } else if (direction == LEFT) {
            head.position = ccp(oldHeadPosition.x - speed * dt, oldHeadPosition.y);
        }
    }

    for (int i = [segments count] - 1; i >= 0; i--) {
        CCSprite *segment = [segments objectAtIndex:i];
        if (i == 0) {
            //NSLog(@"old head (%f,%f)", oldHeadPosition.x, oldHeadPosition.y);
            //NSLog(@"new head (%f,%f)", head.position.x, head.position.y);
            //NSLog(@"\n");
            segment.position = head.position;
        } else {
            CCSprite *next = [segments objectAtIndex:i-1];
            segment.position = next.position;
        }
     }
    
    // extract old tail
    //CCSprite *tail = [segments objectAtIndex:[segments count] - 1];
    //[segments removeLastObject];
    
    // insert new head
    //[segments insertObject:head atIndex:0];
    //[self addChild:head];
    //[self removeChild:tail];
    
    // move every segment up by one

}

// WE SHOULD MOD THESE THINGS
-(void) turnClockwise: (bool) clockwise {
    if (clockwise) {
        if (direction == UP) {
            nextDirection = RIGHT;
            nextNextDirection = RIGHT;
        } else if (direction == RIGHT) {
            nextDirection = DOWN;
            nextNextDirection = DOWN;
        } else if (direction == DOWN) {
            nextDirection = LEFT;
            nextNextDirection = LEFT;
        } else if (direction == LEFT) {
            nextDirection = UP;
            nextNextDirection = UP;
        }
    } else {
        if (direction == UP) {
            nextDirection = LEFT;
            nextNextDirection = LEFT;
        } else if (direction == RIGHT) {
            nextDirection = UP;
            nextNextDirection = LEFT;
        } else if (direction == DOWN) {
            nextDirection = RIGHT;
            nextNextDirection = RIGHT;
        } else if (direction == LEFT) {
            nextDirection = DOWN;
            nextNextDirection = DOWN;
        }
    }
}

-(void) turnClockwise:(bool) clockwise forDoubleTap: (bool) twice {
    if (clockwise) {
        if (direction == UP) {
            nextDirection = RIGHT;
            nextNextDirection = DOWN;
        } else if (direction == RIGHT) {
            nextDirection = DOWN;
            nextNextDirection = LEFT;
        } else if (direction == DOWN) {
            nextDirection = LEFT;
            nextNextDirection = UP;
        } else if (direction == LEFT) {
            nextDirection = UP;
            nextNextDirection = RIGHT;
        }
    } else {
        if (direction == UP) {
            nextDirection = LEFT;
            nextNextDirection = DOWN;
        } else if (direction == RIGHT) {
            nextDirection = UP;
            nextNextDirection = LEFT;
        } else if (direction == DOWN) {
            nextDirection = RIGHT;
            nextNextDirection = UP;
        } else if (direction == LEFT) {
            nextDirection = DOWN;
            nextNextDirection = RIGHT;
        }
    }
}

-(void) changeLength: (int) delta {
    
}

-(CGPoint) getHead {
    return ccp(0,0);
}

@end
