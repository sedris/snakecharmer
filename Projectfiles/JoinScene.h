//
//  JoinScene.h
//  SnakeCharmer
//
//  Created by Sarah Edris on 1/29/14.
//
//

#import "CCScene.h"

@interface JoinScene : CCScene {
    NSString* peerName;
    CCLabelTTF *peerLabel;
}
-(void) changePeers:(NSString *)p;
@end
