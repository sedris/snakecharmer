//
//  GameData.h
//  SnakeCharmer
//
//  Created by Sarah Edris on 1/20/14.
//
//

#import <Foundation/Foundation.h>

@interface GameData : NSObject

//the keyword "nonatomic" is a property declaration
//nonatomic properties have better performance than atomic properties (so use them!)
@property (nonatomic) int WIDTH_WINDOW;
@property (nonatomic) int HEIGHT_WINDOW;
@property (nonatomic) int CELL_DIMENSION;
@property (nonatomic) int Y_OFFSET;
@property (nonatomic) int NUM_COLS;
@property (nonatomic) int NUM_ROWS;
@property (nonatomic) NSString *playerName;

//Static (class) method:
+(GameData*) sharedData;

@end
