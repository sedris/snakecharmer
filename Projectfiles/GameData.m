//
//  GameData.m
//  SnakeCharmer
//
//  Created by Sarah Edris on 1/20/14.
//
//

#import "GameData.h"

@implementation GameData

@synthesize WIDTH_WINDOW;
@synthesize HEIGHT_WINDOW;
@synthesize CELL_DIMENSION;
@synthesize Y_OFFSET;
@synthesize NUM_COLS;
@synthesize NUM_ROWS;

//static variable - this stores our singleton instance
static GameData *sharedData = nil;

+(GameData*) sharedData
{
    //If our singleton instance has not been created (first time it is being accessed)
    if(sharedData == nil)
    {
        //create our singleton instance
        sharedData = [[GameData alloc] init];
        
        //collections (Sets, Dictionaries, Arrays) must be initialized
        //Note: our class does not contain properties, only the instance does
        //self.arrayOfDataToBeStored is invalid
        //sharedData.arrayOfDataToBeStored = [[NSMutableArray alloc] init];
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.height;
        // might need to be . instead of -> ... (but sharedData is a pointer so should be ->)
        sharedData.WIDTH_WINDOW = screenWidth;
        sharedData.HEIGHT_WINDOW = screenHeight;
        sharedData.CELL_DIMENSION = 20;
        sharedData.Y_OFFSET = 60;
        sharedData.NUM_COLS = sharedData.WIDTH_WINDOW/sharedData->CELL_DIMENSION - 1;
        sharedData.NUM_ROWS = (sharedData.HEIGHT_WINDOW - sharedData->Y_OFFSET)/sharedData->CELL_DIMENSION - 1;
        sharedData.playerName = nil;
    }
    
    //if the singleton instance is already created, return it
    return sharedData;
}

@end
